package itacademy.com.project011;

import android.os.PersistableBundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button btnShow = findViewById(R.id.btnShow);
        btnShow.setOnClickListener(this);

        Button btnCallDialog = findViewById(R.id.btnCallDialog);
        btnCallDialog.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnCallDialog:
                DialogNameFragment.newInstance("John Smith").show(getSupportFragmentManager(), "DialogName");
                break;
            case R.id.btnShow:
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                ResultFragment fragment = new ResultFragment();
                fragment.setArguments(getData());
                transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                transaction.replace(R.id.fragmentContainer, fragment).commit();
                break;
        }
    }

    private Bundle getData() {
        Bundle bundle = new Bundle();
        UserModel model = new UserModel();
        model.setName("John Smith");
        model.setAge(35);
        bundle.putSerializable("model", model);
        return bundle;
    }
}
