package itacademy.com.project011;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class DialogNameFragment extends DialogFragment implements View.OnClickListener {

    private TextView tvName;
    private Button btnNo, btnYes;
    private EditText etName;

    public static DialogNameFragment newInstance(String name) {
        DialogNameFragment dialog = new DialogNameFragment();
        Bundle bundle = new Bundle();
        bundle.putString("name", name);
        dialog.setArguments(bundle);
        return dialog;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_dialog_name, container, false);

        etName = view.findViewById(R.id.etName);
        btnNo = view.findViewById(R.id.btnCancel);
        btnYes = view.findViewById(R.id.btnYes);
        tvName = view.findViewById(R.id.tvName);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        btnYes.setOnClickListener(this);
        btnNo.setOnClickListener(this);

        String name = getArguments().getString("name");
        tvName.setText(getFormattedName(name));
    }

    private String getFormattedName(String name) {
        return String.format("Your name is %s", name);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnYes:
                String newName = etName.getText().toString();
                if (!newName.isEmpty()) {
                    tvName.setText(getFormattedName(newName));
                } else {
                    Toast.makeText(getContext(), "New name is empty", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.btnCancel:
                etName.setVisibility(View.VISIBLE);
                break;
        }
    }
}
