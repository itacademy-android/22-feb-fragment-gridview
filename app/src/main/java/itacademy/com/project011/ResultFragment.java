package itacademy.com.project011;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class ResultFragment extends Fragment {

    private TextView tvResult;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_result, container, false);
        tvResult = view.findViewById(R.id.tvResult);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        UserModel model = (UserModel) getArguments().getSerializable("model");
        if (model != null) {
            String name = model.getName();
            String age = String.valueOf(model.getAge());
            String result = String.format("%1s - %2s", name, age);
            tvResult.setText(result);
        }
    }
}
